const { resolve } = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
// const VueLoaderPlugin = require("vue-loader/lib/plugin");
const { VueLoaderPlugin } = require("vue-loader");

module.exports = {
  mode: "development",
  entry: "./src/main.js",
  output: {
    path: resolve(__dirname, "dist"),
    filename: "bundle.js",
  },
  devtool: "source-map",
  // 表示需要使用外部的Vue
  externals: {
    vue: "Vue",
  },

  resolve: {
    extensions: [".js", ".jsx", ".vue"],
  },
  devServer: {
    port: 3333,
  },
  module: {
    rules: [
      {
        test: /\.vue$/i,
        loader: "vue-loader",
      },
      {
        test: /\.scss$/i,
        use: ["vue-style-loader", "css-loader", "sass-loader"],
      },
    ],
  },
  plugins: [
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      template: resolve(__dirname, "public/index.html"),
    }),
  ],
};
