// console.log("hello");
// import App from "./App.vue";
// new Vue({
//   render: (h) => h(App),
// }).$mount("#app");
const app = Vue.createApp({
  data() {
    return {
      title: "This is my TITLE",
    };
  },

  template: `
    <h1> {{ title }} </h1>
  `,
});

const vm = app.mount("#app");
console.log(vm);
