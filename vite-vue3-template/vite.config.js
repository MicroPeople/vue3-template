import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import eslintPlugin from "vite-plugin-eslint";
// import babel from 'vite-babel-plugin';
// import legacy from "@vitejs/plugin-legacy";
import { loadEnv } from "vite";
import { resolve } from "path";
import OptimizationPersist from "vite-plugin-optimize-persist";
import PkgConfig from "vite-plugin-package-config";

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  console.log("mode", mode);
  return {
    plugins: [
      vue(),
      eslintPlugin(),
      // https://github.com/vitejs/vite/tree/main/packages/plugin-legacy
      /* 利用了babel, 来解决转成ES5的代码，因为esbuild 最低只支持到ES6 */
      // legacy({
      //   targets: ["defaults", "not IE 11"],
      // }),
      // https://github.com/antfu/vite-plugin-optimize-persist
      PkgConfig(),
      OptimizationPersist(),
    ],
    server: {
      // host: "0.0.0.0" /* 指定服务器主机名 */,
      port: parseInt(
        loadEnv(mode, process.cwd()).VITE_APP_PORT
      ) /* 指定服务器端口 */,
      strictPort: true /* 设为 true 时若端口已被占用则会直接退出 */,
      // https: true,
      // open: "/" /* 在服务器启动时自动在浏览器中打开应用程序 */,
      hmr: {
        overlay: false /* 为 false 可以禁用服务器错误遮罩层 */,
      },
      proxy: {
        // 字符串简写写法
        "/foo": "http://localhost:4567/foo",
        // 选项写法
        "/api": {
          target: "https://jsonplaceholder.typicode.com/todos",
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, ""),
        },
        "/api2": {
          target: "https://jsonplaceholder.typicode.com/users",
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api2/, ""),
        },
        // 正则表达式写法
        "^/fallback/.*": {
          target: "https://jsonplaceholder.typicode.com/todos",
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/fallback/, ""),
        },
      },
    },
    resolve: {
      /* 设置路径别名 */
      alias: {
        src: resolve(__dirname, "src"),
        "/images": "src/assets/images",
      },
      // extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json'] /* 默认这些，不建议.vue */
    },
    css: {
      /* CSS 预处理器 */
      preprocessorOptions: {
        scss: {
          additionalData: '@import "src/assets/styles/global.scss";',
        },
      },
    },
  };
});
