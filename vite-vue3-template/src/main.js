import { createApp } from "vue";
import App from "./App.vue";

var vm = createApp(App).mount("#app");
vm.studentInfo = "李四";

// import myVue from "./myVue";

// var vm = new myVue({
//   el: "#app",
//   data() {
//     return {
//       a: 1,
//       b: 2,
//     };
//   },
//   template: `
//             <span>{{ a }}</span>
//             <span>+</span>
//             <span>{{ b }}</span>
//             <span>=</span>
//             <span>{{ total }}</span>
//         `,
//   computed: {
//     total() {
//       console.log("computed");
//       return this.a + this.b;
//     },
//     // total: {
//     //     get() {
//     //         console.log('computed')
//     //         return this.a + this.b
//     //     }
//     // }
//   },
// });

// console.log(vm);

// console.log(vm.total);
// console.log(vm.total);
// console.log(vm.total);

// vm.a = 100;
// vm.b = 100;

// console.log(vm.total);
// console.log(vm.total);
// console.log(vm.total);
