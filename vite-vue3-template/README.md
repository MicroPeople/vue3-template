# Vue 3 + Vite

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)

```js
var myVue = (function () {
  /**
   * computedData
   * store the value, method and dependency data of computed properties
   * {
   *  value: value,
   *  get: fn,
   *  dep: [a, b, ...]
   * }
   */
  var computedData = {},
    dataPool = {};

  function myVue(options) {
    this.$el = document.querySelector(options.el);
    this.$data = options.data();

    this._init(this, options.computed, options.template);
  }

  myVue.prototype._init = function (vm, computed, template) {
    dataReactive(vm);
    computedReactive(vm, computed);
    render(vm, template);
  };
  
  function dataReactive(vm) {
    var _data = vm.$data;

    for (var key in _data) {
      (function (key) {
        Object.defineProperty(vm, key, {
          get() {
            return _data[key];
          },
          set(newValue) {
            _data[key] = newValue;
            update(vm, key);
            _updateComputedData(vm, key, function (key) {
              update(vm, key);
            });
          },
        });
      })(key);
    }
  }

  function computedReactive(vm, computed) {
    _initComputedData(vm, computed);

    // watch the value, mount computed property 'total' to 'vm' object
    for (var key in computedData) {
      (function (key) {
        Object.defineProperty(vm, key, {
          get() {
            return computedData[key].value;
          },
          set(newValue) {
            computedData[key].value = newValue;
          },
        });
      })(key);
    }
  }

  function render(vm, template) {
    // construct container
    var container = document.createElement("div"),
      _el = vm.$el;

    container.innerHTML = template;

    // replace the content of template
    var domTree = _compileTemplate(vm, container);
    // 插入到app中
    _el.appendChild(domTree);
  }

  function update(vm, key) {
    dataPool[key].textContent = vm[key];
  }

  function _compileTemplate(vm, container) {
    // 获取container里面所有的元素
    var allNodes = container.getElementsByTagName("*"),
      item = null,
      var_reg = /\{\{(.+?)\}\}/g; // 匹配模板插值 {{ xxx }}

    for (var i = 0; i < allNodes.length; i++) {
      item = allNodes[i];

      // 匹配出模板中所使用的变量 {{ a }}
      var matched = item.textContent.match(var_reg);

      if (matched) {
        item.textContent = item.textContent.replace(
          var_reg,
          function (node, key) {
            dataPool[key.trim()] = item;

            return vm[key.trim()];
          }
        );
      }
    }

    return container;
  }

  function _updateComputedData(vm, key, update) {
    var _dep = null;

    for (var _key in computedData) {
      _dep = computedData[_key].dep;

      // 对比数据 更新
      for (var i = 0; i < _dep.length; i++) {
        if (_dep[i] === key) {
          vm[_key] = computedData[_key].get();
          update(_key);
        }
      }
    }
  }

  function _initComputedData(vm, computed) {
    for (var key in computed) {
      var descriptor = Object.getOwnPropertyDescriptor(computed, key), // 获取属性描述符 value configurable...
        descriptorFn = descriptor.value.get || descriptor.value;

      computedData[key] = {};
      computedData[key].value = descriptorFn.call(vm); // 计算属性首次计算出来的值
      computedData[key].get = descriptorFn.bind(vm); // 计算属性的方法
      computedData[key].dep = _collectDep(descriptorFn); // 收集计算属性方法所依赖的数据
    }
  }

  function _collectDep(fn) {
    // 将计算属性方法转成字符串 收集其中this.xxx数据
    var _collection = fn.toString().match(/this.(.+?)/g);

    if (_collection.length) {
      for (var i = 0; i < _collection.length; i++) {
        // 转成变量名 a b...
        _collection[i] = _collection[i].split(".")[1];
      }
    }

    return _collection;
  }

  return myVue;
})();

export default myVue;

```

### MarkdownLint

> Markdownlint  
