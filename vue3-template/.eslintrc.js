module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: ["eslint:recommended", "plugin:vue/essential", "plugin:prettier/recommended"],
  parserOptions: {
    ecmaVersion: 10,
    sourceType: "module",
    ecmaFeatures: {
      modules: true,
    },
  },
  plugins: ["vue"],
  rules: {
    "no-undef": "off",
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "vue/no-multiple-template-root": "off",
    "vue/multi-word-component-names": "off",
    // 强制使用驼峰命名
    "vue/component-name-in-template-casing": [
      "error",
      "PascalCase",
      {
        registeredComponentsOnly: false,
        ignores: [],
      },
    ],

    "space-before-function-paren": 0,
    "prettier/prettier": [
      "error",
      {
        endOfLine: "auto", // 取消行尾检测 结尾是 \n \r \n\r auto
        printWidth: 100, // 指定代码长度，超出换行
        proseWrap: "preserve", // 按照文件原样折行
        htmlWhitespaceSensitivity: "ignore", // html文件的空格敏感度，控制空格是否影响布局
        tabWidth: 2,
        useTabs: false, // 不使用tab
        semi: true, // 结尾加上分号
        singleQuote: false, // 不使用单引号
        quoteProps: "as-needed", // 要求对象字面量属性是否使用引号包裹,(‘as-needed’: 没有特殊要求，禁止使用，'consistent': 保持一致 , preserve: 不限制，想用就用)
        jsxSingleQuote: false, // jsx 语法中使用单引号
        trailingComma: "es5", // 确保对象的最后一个属性后有逗号
        bracketSpacing: true, // 大括号有空格 { name: 'rose' }
        arrowParens: "always", // 箭头函数，单个参数添加括号
      },
    ],
  },
  globals: {
    defineProps: "readonly",
    defineEmits: "readonly",
    defineExpose: "readonly",
    withDefaults: "readonly",
  },
};
