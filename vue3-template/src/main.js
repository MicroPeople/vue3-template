import { createApp } from "vue";
import App from "./App.vue";
const app = createApp(App);

const vm = app.mount("#app");

// console.log(vm);
// console.log(vm.$data.title);
// console.log(vm.title);
// vm.title = "TTT";

// $data响应式数据对象
// vm.$data.author = "KAI"; // 可以渲染
// vm.author = "KAI"; // 不行

// // data为什么必须是一个函数
// const MyComponent = {
//   // Uncaught TypeError: dataOptions.call is not a function
//   data() {
//     return {
//       a: 1,
//     };
//   },
//   template: `
//   <div>
//       <h1>{{ a }}</h1>
//   </div>

//   `,
// };
// const app = createApp(MyComponent);
// const vm = app.mount("#app");
console.log(vm);
